import React from "react";
import { Row, Col } from "react-bootstrap";

export const Team = () => (
    <section id='team' className='section bg-3'>
        <div className='section__container--width1200 text--white '>
            <div className=''>
                <h1 className='section__header text--center text--white'>Nasz zespół</h1>

                <Row>
                    <Col sm={6}>
                        <h4 className='section__subheader'>
                            Opiekun: <strong>Duda</strong>
                        </h4>
                        <div class='section__text'>
                            rem ipsum dolor sit amet, consectetur adipiscing elit. Ut vehicula sagittis erat, non
                            hendrerit erat bibendum elementum.
                        </div>
                    </Col>
                    <Col sm={6}>
                        <h4 className='section__subheader'>
                            Prezes: <strong>Tomasz Wieroński</strong>
                        </h4>
                        <div class='section__text'>
                            rem ipsum dolor sit amet, consectetur adipiscing elit. Ut vehicula sagittis erat, non
                            hendrerit erat bibendum elementum.
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col sm={6}>
                        <h4 className='section__subheader'>
                            Wice: <strong>Joasia</strong>
                        </h4>
                        <div class='section__text'>
                            rem ipsum dolor sit amet, consectetur adipiscing elit. Ut vehicula sagittis erat, non
                            hendrerit erat bibendum elementum.
                        </div>
                    </Col>
                    <Col sm={6}>
                        <h4 className='section__subheader'>
                            Od strony: <strong>Maciej Zając</strong>
                        </h4>
                        <div class='section__text'>
                            rem ipsum dolor sit amet, consectetur adipiscing elit. Ut vehicula sagittis erat, non
                            hendrerit erat bibendum elementum.
                        </div>
                    </Col>
                </Row>
            </div>
        </div>
    </section>
);
