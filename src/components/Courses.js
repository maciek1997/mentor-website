import React from "react";
import { Row, Col } from "react-bootstrap";

export const Courses = () => (
    <section name='courses' id='courses' className='section'>
        <div className='section__container section__container--width1200'>
            <h1 className='section__header text--center'>Linki do kursów</h1>
            <Row>
                <Col sm={6}>
                    <h3 className='section__subheader'>Javascript</h3>
                    <div className='section__text'>
                        <h4>
                            <a href='https://javascript.info/' className='section__link'>
                                Podstawy javascript
                            </a>
                        </h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vehicula sagittis erat, non
                            hendrerit erat bibendum elementum.
                        </p>
                    </div>
                    <div className='section__text'>
                        <h4>
                            <a href='https://www.freecodecamp.org/' className='section__link'>
                                Freecodecamp
                            </a>
                        </h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vehicula sagittis erat, non
                            hendrerit erat bibendum elementum.
                        </p>
                    </div>
                    <div className='section__text'>
                        <h4>
                            <a href='https://www.w3schools.com/' className='section__link'>
                                w3schools
                            </a>
                        </h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vehicula sagittis erat, non
                            hendrerit erat bibendum elementum.
                        </p>
                    </div>
                </Col>
                <Col sm={6}>
                    <h3 className='section__subheader'>HTML/CSS</h3>
                    <div className='section__text'>
                        <h4>
                            <a href='https://www.w3schools.com/' className='section__link'>
                                w3schools
                            </a>
                        </h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vehicula sagittis erat, non
                            hendrerit erat bibendum elementum.
                        </p>
                    </div>
                    <div className='section__text'>
                        <h4>
                            <a href='https://javascript30.com/' className='section__link'>
                                Javascript30
                            </a>
                        </h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vehicula sagittis erat, non
                            hendrerit erat bibendum elementum.
                        </p>
                    </div>
                </Col>
            </Row>

            <Row className='padding-top-20-px'>
                <Col sm={6}>
                    <h3 className='section__subheader'>C#</h3>
                    <div className='section__text'>
                        <h4>
                            <a href='https://javascript.info/' className='section__link'>
                                Podstawy javascript
                            </a>
                        </h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vehicula sagittis erat, non
                            hendrerit erat bibendum elementum.
                        </p>
                    </div>
                </Col>
                <Col sm={6}>
                    <h3 className='section__subheader'>ASP.NET</h3>
                    <div className='section__text'>
                        <h4>
                            <a href='https://www.w3schools.com/' className='section__link'>
                                w3schools
                            </a>
                        </h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vehicula sagittis erat, non
                            hendrerit erat bibendum elementum.
                        </p>
                    </div>
                </Col>
            </Row>
        </div>
    </section>
);
