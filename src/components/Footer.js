import React from "react";
import MentorLogo from "../images/mentor.png";
import { Row, Col } from "react-bootstrap";

export const Footer = () => {
    const year = new Date().getFullYear();
    return (
        <footer id='contact' className='section position--relative'>
            <div>
                <div className=''>
                    <h1 className='section__header text--center'>Kontakt</h1>
                    <Row className='flex text--center'>
                        <Col sm={4}>
                            <a href='#'>
                                <img src={MentorLogo} className='img-fluid' alt='Mentor logo' />
                            </a>
                        </Col>
                        <Col sm={4}>
                            <p className='section__text'>
                                Wydział Zarządzania
                                <br />
                                Akademia Górniczo-Hutnicza im. Stanisława Staszica
                                <br />
                                ul. Gramatyka xx xx-xxx Kraków <br />
                                NIP AGH: xxxxxxxxx
                                <br />
                                xxxxxxx@gmail.com
                            </p>
                        </Col>
                        <Col sm={4}>
                            <div>
                                <a href='#' className='section__icon'>
                                    <i className='fa fa-twitter fa-2x' style={{ color: "black" }} />
                                </a>
                                <a href='#' className='section__icon'>
                                    <i className='fa fa-facebook fa-2x' style={{ color: "black" }} />
                                </a>
                                <a href='#' className='section__icon'>
                                    <i className='fa fa-google-plus fa-2x' style={{ color: "black" }} />
                                </a>
                            </div>
                            <button type='button' className='btn btn-default'>
                                <a href='mailto:abs@gmail.com' className='text-white'>
                                    Kontakt
                                </a>
                            </button>
                        </Col>
                    </Row>
                    <div className='section__footer'>
                        <div className=''>Maciej Zając {year} © Copyright </div>
                    </div>
                </div>
            </div>
        </footer>
    );
};
