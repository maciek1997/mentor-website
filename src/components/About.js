import React from "react";

export const About = () => (
    <section id='about' className='section'>
        <div className='section__container'>
            <h1 className='section__header text-center'>Kim jesteśmy</h1>
            <hr />
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vehicula sagittis erat, non hendrerit erat
                bibendum elementum. Nam faucibus lacus nec tortor euismod, non aliquam massa dictum. Sed consectetur
                libero vel dapibus condimentum
            </p>
        </div>
    </section>
);
