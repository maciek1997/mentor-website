import React from "react";
import { Navbar } from "./Navbar";
import { LandingPage } from "./LandingPage";
import { About } from "./About";
import { News } from "./News";
import { Courses } from "./Courses";
import { Team } from "./Team";
import { Footer } from "./Footer";

export const App = () => {
    return (
        <>
            <Navbar />
            <LandingPage />
            <About />
            <News />
            <Courses />
            <Team />
            <Footer />
        </>
    );
};
