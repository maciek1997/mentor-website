import React from "react";
import MentorLogo from "../images/mentor.png";

export const LandingPage = () => {
    return (
        <div id='mentor'>
            <div class='welcome__page'>
                <img src={MentorLogo} alt='mentorLogo' class='welcome__logo' />
            </div>
        </div>
    );
};
