import React from "react";
import { NavItem } from "./NavItem";

export const Navbar = () => {
    const leftList = [
        ["about", "O nas"],
        ["news", "Aktualności"],
        ["courses", "Kursy"],
        ["team", "Zespół"],
        ["contact", "Kontakt"]
    ];

    return (
        <nav className='navbar fixed-top navbar-expand-md navbar__background'>
            <a className='navbar__brand' href='#mentor'>
                Mentor
            </a>
            <button
                className='navbar-toggler'
                type='button'
                data-toggle='collapse'
                data-target='#navbarNavDropdown'
                aria-controls='navbarNavDropdown'
                aria-expanded='false'
                aria-label='Toggle navigation'
            >
                <span className='navbar-toggler-icon' />
            </button>
            <div className='collapse navbar-collapse' id='navbarNavDropdown'>
                <ul className='navbar-nav mr-auto'>
                    {leftList.map(item => (
                        <NavItem link={item[0]} text={item[1]} />
                    ))}
                </ul>

                <ul className='navbar-nav ml-auto'>
                    <li className='navbar__item'>
                        <a className='navbar__link' href='#'>
                            <i className='fa fa-facebook' />
                        </a>
                    </li>
                    <li className='navbar__item'>
                        <a className='navbar__link' href='#'>
                            <i className='fa fa-github' />
                        </a>
                    </li>
                    <li className='navbar__item'>
                        <a className='navbar__link' href='#'>
                            <i className='fa fa-dropbox' />
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    );
};
