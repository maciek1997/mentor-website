import React from "react";
import { Col, Row } from "react-bootstrap";

export const News = () => {
    const newsList = [
        ["Nasze spotkania odbywają się w każdą środę o godzinie 16:45 w sali 307.", "2018-10-17"],
        ["Lorem ipsum Lorem ipsum Lorem ipsum", "2019-01-01"],
        ["Lorem ipsum Lorem ipsum Lorem ipsum", "2019-01-01"],
        ["Lorem ipsum Lorem ipsum Lorem ipsum", "2019-01-01"]
    ];
    return (
        <section id='news' className='section'>
            <div className='section__container text--center'>
                <div className='section__header'>
                    <h2 className=''>Aktualności</h2>
                </div>
                {newsList.map(item => (
                    <Row>
                        <Col md={9}>
                            <h5>{item[0]}</h5>
                        </Col>
                        <Col md={3}>
                            <p className=''>{item[1]}</p>
                        </Col>
                    </Row>
                ))}
            </div>
        </section>
    );
};
