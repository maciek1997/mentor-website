import React from "react";
import { Link } from "react-scroll";

export const NavItem = ({ link, text }) => (
    <li class='navbar__item'>
        <Link className='navbar__link' spy={true} to={`${link}`} smooth={true}>
            {text}
        </Link>
    </li>
);
